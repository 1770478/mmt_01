package com.hcmut.vo;

import java.sql.Timestamp;

public class PushData {
    private String title;
    private String message;
    private boolean is_background = true;
    private String image = "";
    private Timestamp timestamp;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isIs_background() {
        return is_background;
    }

    public void setIs_background(boolean is_background) {
        this.is_background = is_background;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}
