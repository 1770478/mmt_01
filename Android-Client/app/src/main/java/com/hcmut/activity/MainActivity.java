package com.hcmut.activity;

import android.app.Activity;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.PersistableBundle;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.hcmut.BuildConfig;
import com.hcmut.R;
import com.google.firebase.messaging.FirebaseMessaging;
import com.hcmut.job.ScheduleJob;
import com.hcmut.notifications.app.Config;
import com.hcmut.notifications.utils.NotificationUtils;
import com.hcmut.service.TcpServer;


public class MainActivity extends AppCompatActivity {
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private TextView txtMessage;
    private Button btnJoin;
    private Button btnLeave;
    private JobScheduler jobScheduler;
    private EditText txtName;
    private String regId = "";
    private TcpServer tcpServer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);

        txtName = (EditText) findViewById(R.id.name);
        btnJoin = (Button)findViewById(R.id.join);
        btnLeave = (Button)findViewById(R.id.leave);

        btnJoin.setOnClickListener(join());

        btnLeave.setOnClickListener(leave());

        txtMessage = (TextView) findViewById(R.id.txt_push_message);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    String message = intent.getStringExtra("message");

                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();

                    txtMessage.setText(message);
                }
            }
        };
        displayFirebaseRegId();
        cancelAll();
        /*tcpServer = new TcpServer(getApplicationContext());
        new Thread(tcpServer).start();*/
    }

    private View.OnClickListener join() {
        return new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                String name = txtName.getText().toString();
                if (name != null) {
                    name.replaceAll("\\s+","");
                }

                if (name.matches("")) {
                    Toast.makeText(getApplicationContext(), "Vui lòng nhập tên !!!", Toast.LENGTH_SHORT).show();
                    return;
                }

                PersistableBundle bundle = new PersistableBundle();
                bundle.putString("name", name);
                bundle.putString("regId", regId);
                JobInfo.Builder builder = new JobInfo.Builder(1, new ComponentName(getPackageName(),
                        ScheduleJob.class.getName()));
                builder.setExtras(bundle);
                builder.setPeriodic(BuildConfig.TIME_SEND);
                jobScheduler.schedule(builder.build());

                btnJoin.setVisibility(View.GONE);
                btnLeave.setVisibility(View.VISIBLE);

                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                disableEditText(txtName);
            }
        };
    }

    private void enableEditText(EditText editText) {
        editText.setEnabled(true);
        editText.setCursorVisible(true);
        editText.setBackgroundColor(Color.TRANSPARENT);
    }

    private void disableEditText(EditText editText) {
        editText.setEnabled(false);
        editText.setCursorVisible(false);
        editText.setBackgroundColor(Color.TRANSPARENT);

    }

    private View.OnClickListener leave() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jobScheduler.cancelAll();

                btnLeave.setVisibility(View.GONE);
                btnJoin.setVisibility(View.VISIBLE);
                enableEditText(txtName);
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    private void cancelAll() {
        try {
            jobScheduler.cancelAll();
            /*tcpServer.stop();*/
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    @Override
    protected void onDestroy() {
        cancelAll();
        super.onDestroy();
    }

    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
        this.regId = regId;
    }
}
