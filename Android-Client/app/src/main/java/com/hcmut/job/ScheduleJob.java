package com.hcmut.job;
/**
 * Created by Long Dang on 12/3/2017.
 */

import android.annotation.SuppressLint;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;

import com.hcmut.service.UdpClient;

import java.util.Date;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
@SuppressLint("MissingPermission")
public class ScheduleJob extends JobService implements LocationListener {
    private JobParameters params;
    private LocationManager locationManager;

    @Override
    public boolean onStartJob(JobParameters params) {
        this.params = params;

        if (locationManager == null) {
            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
             locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        }

        new SendDataTask().execute(params);
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }

    private class SendDataTask extends AsyncTask<JobParameters, Void, Void> {

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            jobFinished(params, false);
        }

        @Override
        protected Void doInBackground(JobParameters... params) {
            UdpClient client = new UdpClient();
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (location != null) {
                if (params.length > 0) {
                    String name = params[0].getExtras().getString("name");
                    String regId = params[0].getExtras().getString("regId");
                    String message = createMessage(name, regId, location.getLatitude(), location.getLongitude());
                    client.sendMessage(message);
                    /*String message1 = createMessage(name + "1", regId + "1", location.getLatitude() + 20, location.getLongitude() + 30);
                    client.sendMessage(message1);*/
                }
            }

            client.close();
            return null;
        }

        private String createMessage(String name, String regId, double latitude, double longitude) {
            StringBuilder sb = new StringBuilder();
            sb.append("name ");
            sb.append(name);
            sb.append("\t");

            sb.append("id ");
            sb.append(regId);
            sb.append("\t");

            sb.append("latitude ");
            sb.append(latitude);
            sb.append("\t");

            sb.append("longitude ");
            sb.append(longitude);
            sb.append("\t");

            sb.append("time ");
            sb.append(new Date().getTime());

            return sb.toString();
        }
    }


    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}