package com.hcmut.service;

import android.content.Context;
import android.util.Log;

import com.google.common.base.Splitter;
import com.hcmut.BuildConfig;
import com.hcmut.vo.PushData;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Timestamp;
import java.util.Map;

/**
 * Created by Long Dang on 12/8/2017.
 */

public class TcpServer implements Runnable {
    public static final int SERVERPORT = BuildConfig.TCP_PORT;
    private ServerSocket server;
    private Context context;
    private BufferedReader in;
    private Socket socket;
    public TcpServer(Context context) {
        this.context = context;
    }

    @Override
    public void run() {
        try {
            server = new ServerSocket(SERVERPORT);

            while (true) {
                socket = server.accept();
                in = new BufferedReader(
                        new InputStreamReader(socket.getInputStream()));
                String message = in.readLine();
                PushData pushData = createPushData(message);
                MessagingService messagingService = new MessagingService();
                messagingService.onMessageReceived(context, pushData);
                in.close();
                socket.close();
            }
        } catch (IOException e) {
            Log.e("Tcp server: ", e.getMessage());
        } catch (Exception e) {
            Log.e("Tcp server: ", e.getMessage());
        }
    }

    public void stop() throws IOException {
        in.close();
        socket.close();
    }

    private PushData createPushData(String message) {
        Map<String, String> properties = Splitter.on("\t").withKeyValueSeparator(":").split(message);
        PushData pushData = new PushData();
        pushData.setTitle(properties.get("title"));
        pushData.setMessage(properties.get("message"));
        pushData.setImage(properties.get("image"));
        pushData.setIs_background(Boolean.parseBoolean(properties.get("background")));
        pushData.setTimestamp(new Timestamp(Long.parseLong(properties.get("timestamp"))));

        return pushData;
    }
}
