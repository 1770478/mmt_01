package com.hcmut.service;

import com.hcmut.BuildConfig;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UdpClient {
    private DatagramSocket socket;
    private InetAddress address;

    private byte[] buf;

    public UdpClient() {
        try {
            socket = new DatagramSocket();
            address = InetAddress.getByName(BuildConfig.GATEWAY_ADDRESS);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void sendMessage(String msg) {
        DatagramPacket packet = null;
        try {
            buf = msg.getBytes();
            packet = new DatagramPacket(buf, buf.length, address, BuildConfig.GATEWAY_PORT);
            socket.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        socket.close();
    }
}
