package com.hcmut.gateway.schedule;

import com.hcmut.gateway.service.Subscriber;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Component
public class MqttSchedule {
    public static Map<String, String> map = new HashMap<>();

    @Autowired
    private Subscriber subscriber;

    @Scheduled(cron = "0 0/1 * 1/1 * ?")
    public void scheduleTaskWithCronExpression() {
        StringBuffer sb = new StringBuffer();
        if (map != null) {

            Iterator<String> iterator = map.keySet().iterator();

            while(iterator.hasNext()) {
                String key = iterator.next();
                sb.append(map.get(key));

                if (iterator.hasNext()) {
                    sb.append("\n");
                }

            }

            try {
                String message = sb.toString();
                if (!"".equals(message)) {
                    subscriber.sendMessage(sb.toString());
                    map = new HashMap<>();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
