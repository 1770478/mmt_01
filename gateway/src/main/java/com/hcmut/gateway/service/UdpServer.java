package com.hcmut.gateway.service;

import com.google.common.base.Splitter;
import com.hcmut.gateway.schedule.MqttSchedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.HashMap;
import java.util.Map;


public class UdpServer extends Thread {
    @Value("${updPort}")
    private int updPort;

    protected DatagramSocket socket = null;
    protected boolean running;
    protected byte[] buf = new byte[256];

    public UdpServer(int updPort) throws Exception {
        socket = new DatagramSocket(updPort);
    }

    public void run() {
        running = true;
        while (running) {
            try {
                DatagramPacket packet = new DatagramPacket(buf, buf.length);
                socket.receive(packet);
                String received = new String(packet.getData(), 0, packet.getLength());
                processMessage(received, packet.getAddress().getHostAddress());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        socket.close();
    }

    private void processMessage(String message, String address) {
        message += "\t" + "address " + address;
        Splitter.MapSplitter mapSplitter = Splitter.on("\t").withKeyValueSeparator(" ");
        Map<String, String> properties = mapSplitter.split(message);

        String id = properties.get("id");
        MqttSchedule.map.put(id, message);
    }
}