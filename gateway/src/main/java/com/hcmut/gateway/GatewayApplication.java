package com.hcmut.gateway;

import com.hcmut.gateway.service.UdpServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class GatewayApplication implements CommandLineRunner {
	@Autowired
	private Environment environment;

	public static void main(String[] args) throws Exception {
		SpringApplication.run(GatewayApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {
		int updPort = Integer.parseInt(environment.getProperty("updPort"));
		new UdpServer(updPort).run();
	}
}
