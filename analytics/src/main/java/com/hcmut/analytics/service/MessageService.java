package com.hcmut.analytics.service;

import com.google.common.base.Splitter;
import com.google.gson.Gson;
import com.hcmut.analytics.vo.Client;
import com.hcmut.analytics.vo.Location;
import com.hcmut.analytics.vo.Push;
import com.hcmut.analytics.vo.PushData;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class MessageService {
    @Value("${minDistance}")
    private double minDistance;
    @Value("${fcmUrl}")
    private String fcmUrl;
    @Value("${key}")
    private String key;
    @Value("${serverPort}")
    private int serverPort;

    public void processMessage(String message) {
        List<String> messageList = Splitter.on("\n").splitToList(message);
        processMessageList(messageList);
    }

    private void processMessageList(List<String> messageList) {
        List<Client> clients = new ArrayList<>();

        for (String message : messageList) {
            Client client = convertMsg(message);
            clients.add(client);
        }

        List<List<Client>> listGroup = new ArrayList<>();

        for (Client client : clients) {
            boolean hasGroup = false;

            for (List<Client> list : listGroup) {
                hasGroup = hasGroup || checkInGroup(client, list);
            }

            if(!hasGroup) {
                List<Client> clientList = new ArrayList<>();
                clientList.add(client);
                listGroup.add(clientList);
            }
        }

        sendPush(listGroup, clients);
    }

    private void sendPush(List<List<Client>> listGroup, List<Client> allClients) {
        if (listGroup != null && listGroup.size() >= 2) {
            List<String> messageList = new ArrayList<>();

            for (List<Client> clients : listGroup) {
                StringBuilder sb = new StringBuilder();

                int size = clients.size();

                for (int i = 0; i < size; i++) {
                    sb.append(clients.get(i).getName());

                    if (i != size - 1) {
                        sb.append(", ");
                    }

                }

                messageList.add(sb.toString());
            }

            StringBuilder sb = new StringBuilder();

            int size = messageList.size();

            for (int i = 0; i < size; i ++) {
                sb.append(messageList.get(i));

                if (i != size - 1) {
                    sb.append(" và ");
                }
            }

            sb.append(" đang cách xa nhau hơn " + minDistance + " mét");

            for (Client client : allClients) {
                sendPush(sb.toString(), client);
            }
        }
    }

    private void sendPush(String message, Client client) {
        Push push = new Push();
        push.setTo(client.getId());
        PushData pushData = new PushData();
        pushData.setMessage(message);
        pushData.setTimestamp(client.getDate());
        pushData.setTitle("Thông báo");
        push.setData(pushData);

        sendToFcm(push);
        //sendToSocket(push);
    }

    private void sendToSocket(Push push) {
        String message = createMessageSocket(push);
        TcpClient client = new TcpClient();
        try {
            client.startConnection(push.getTo(), serverPort);
            client.sendMessage(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String createMessageSocket(Push push) {
        StringBuilder sb = new StringBuilder();
        sb.append("title:");
        sb.append(push.getData().getTitle());
        sb.append("\t");

        sb.append("message:");
        sb.append(push.getData().getMessage());
        sb.append("\t");

        sb.append("background:");
        sb.append(push.getData().isIs_background());
        sb.append("\t");

        sb.append("image:");
        sb.append(push.getData().getImage());
        sb.append("\t");

        sb.append("timestamp:");
        sb.append(push.getData().getTimestamp().getTime());

        return sb.toString();
    }

    private void sendToFcm(Push push) {
        HttpClient httpclient = HttpClientBuilder.create().build();
        StringEntity requestEntity = new StringEntity(
                new Gson().toJson(push),
                ContentType.APPLICATION_JSON);

        HttpPost postMethod = new HttpPost(fcmUrl);
        postMethod.setHeader("Authorization", key);
        postMethod.setEntity(requestEntity);

        try {
            httpclient.execute(postMethod);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private boolean checkInGroup(Client client, List<Client> clients) {
        for (Client c : clients) {
            if (calculatorDistance(c.getLocation(), client.getLocation()) < minDistance) {
                return true;
            }
        }

        return false;
    }

    private Client convertMsg(String message) {
        Map<String, String> properties = Splitter.on("\t").withKeyValueSeparator(" ").split(message);
        Client client = new Client();
        client.setId(properties.get("id"));
        //client.setId(properties.get("address"));
        client.setName(properties.get("name"));

        Location location = new Location();
        location.setLatitude(Double.parseDouble(properties.get("latitude")));
        location.setLongitude(Double.parseDouble(properties.get("longitude")));
        client.setLocation(location);

        client.setDate(new Timestamp(Long.parseLong(properties.get("time"))));

        return client;
    }

    private double calculatorDistance(Location l1, Location l2) {
        double R = 6373;

        double lat1 = Math.toRadians(l1.getLatitude());
        double lon1 = Math.toRadians(l1.getLongitude());

        double lat2 = Math.toRadians(l2.getLatitude());
        double lon2 = Math.toRadians(l2.getLongitude());

        double dlon = lon2 - lon1;
        double dlat = lat2 - lat1;

        double a = (Math.sin(dlat/2)) * (Math.sin(dlat/2)) + Math.cos(lat1) * Math.cos(lat2) * (Math.sin(dlon/2)) * (Math.sin(dlon/2));
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = R * c;

        return d * 1000; //meter
    }
}
