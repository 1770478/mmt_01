package com.hcmut.analytics;

import com.hcmut.analytics.service.TcpClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AnalyticsApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Test
	public void givenGreetingClient_whenServerRespondsWhenStarted_thenCorrect() throws IOException {
		TcpClient client = new TcpClient();
		client.startConnection("127.0.0.1", 6000);
		String response = client.sendMessage("hello server");
		assertEquals("hello client", response);
	}
}
